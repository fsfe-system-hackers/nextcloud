# Nextcloud

[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/nextcloud/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/nextcloud)

The FSFE offers a [Nextcloud](https://nextcloud.com) instance for staff and interested core team members on [nextcloud.fsfe.org](https://nextcloud.fsfe.org). The service currently is not meant for file storage but primarily for sharing calendars, contacts and editing files collaboratively.

User documentation can be found in the [FSFE's wiki](https://wiki.fsfe.org/TechDocs/Nextcloud) and the [official help documents](https://nextcloud.com/support/).
